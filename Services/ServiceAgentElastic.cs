﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPressSyncModule.Models;

namespace WordPressSyncModule.Services
{
    public class ServiceAgentElastic
    {
        private Globals globals;

        public async Task<ResultItem<GetSyncWordpressPostsModelResult>> GetSyncWordpressPostsModel(SyncWordpressPostsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetSyncWordpressPostsModelResult>>(() =>
           {
               var result = new ResultItem<GetSyncWordpressPostsModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetSyncWordpressPostsModelResult()
               };

               if (string.IsNullOrEmpty( request.IdConfig))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "config-id is not valid!";
                   return result;
               }

               if (!globals.is_GUID(request.IdConfig))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "config-id is not valid!";
                   return result;
               }

               var searchBaseConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderBaseConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderBaseConfig.GetDataObjects(searchBaseConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Base-config!";
                   return result;
               }

               result.Result.BaseConfig = dbReaderBaseConfig.Objects1.FirstOrDefault();

               if (result.Result.BaseConfig == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Base-config found!";
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.BaseConfig.GUID,
                       ID_RelationType = WordpressSync.Config.LocalData.ClassRel_Get_Wordpress_Posts_contains_Get_Wordpress_Posts.ID_RelationType,
                       ID_Parent_Other = WordpressSync.Config.LocalData.ClassRel_Get_Wordpress_Posts_contains_Get_Wordpress_Posts.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the sub-configs!";
                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.BaseConfig);
               }

               var searchMasterUsers = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = WordpressSync.Config.LocalData.ClassRel_Get_Wordpress_Posts_secured_by_user.ID_RelationType,
                   ID_Parent_Other = WordpressSync.Config.LocalData.ClassRel_Get_Wordpress_Posts_secured_by_user.ID_Class_Right
               }).ToList();

               var dbReaderMasterUsers = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderMasterUsers.GetDataObjectRel(searchMasterUsers);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Masterusers!";
                   return result;
               }

               result.Result.ConfigsToMasterUsers = dbReaderMasterUsers.ObjectRels;

               if (!result.Result.ConfigsToMasterUsers.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Webservices found!";
                   return result;
               }

               var searchWebService = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = WordpressSync.Config.LocalData.ClassRel_Get_Wordpress_Posts_uses_Web_Service.ID_RelationType,
                   ID_Parent_Other = WordpressSync.Config.LocalData.ClassRel_Get_Wordpress_Posts_uses_Web_Service.ID_Class_Right
               }).ToList();

               var dbReaderConfigsToWebservices = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigsToWebservices.GetDataObjectRel(searchWebService);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Webservices!";
                   return result;
               }

               result.Result.WebServices = dbReaderConfigsToWebservices.ObjectRels;

               if (!result.Result.WebServices.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Webservices found!";
                   return result;
               }

               var searchUrl = result.Result.WebServices.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = WordpressSync.Config.LocalData.ClassRel_Web_Service_accessible_by_Url.ID_RelationType,
                   ID_Parent_Other = WordpressSync.Config.LocalData.ClassRel_Web_Service_accessible_by_Url.ID_Class_Right
               }).ToList();

               var dbReaderWebservicesToUrls = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderWebservicesToUrls.GetDataObjectRel(searchUrl);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Urls of the Webservices!";
                   return result;
               }

               result.Result.WebServicesToUrls = dbReaderWebservicesToUrls.ObjectRels;

               if (!result.Result.WebServicesToUrls.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while getting the Urls of Webservices!";
                   return result;
               }

               var searchWebservicesToUsers = result.Result.WebServices.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = WordpressSync.Config.LocalData.ClassRel_Web_Service_secured_by_user.ID_RelationType,
                   ID_Parent_Other = WordpressSync.Config.LocalData.ClassRel_Web_Service_secured_by_user.ID_Class_Right
               }).ToList();

               var dbReaderWebServicesToUsers = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderWebServicesToUsers.GetDataObjectRel(searchWebservicesToUsers);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Users of the Webservices!";
                   return result;
               }

               result.Result.WebServicesToUsers = dbReaderWebServicesToUsers.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals)
        {
            this.globals = globals;
        }
    }
}
