﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;
using WordPress_Module.Factory;
using WordPressSharp;
using WordPressSyncModule.Factories;
using WordPressSyncModule.Models;
using WordPressSyncModule.Services;

namespace WordPressSyncModule
{
    public class WordpressSyncConnector : AppController
    {
        public async Task<ResultItem<KendoDropDownConfig>> GetDropDownConfigForWebservices(List<WebServiceItem> webServices)
        {
            var taskResult = await Task.Run<ResultItem<KendoDropDownConfig>>(() =>
            {
                
                var result = new ResultItem<KendoDropDownConfig>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new KendoDropDownConfig
                    {
                        dataSource = webServices.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                        {
                            Value = attType.Guid,
                            Text = attType.Name
                        }).ToList(),
                        optionLabel = "Service"
                    }
                };
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<KendoDropDownConfig>> GetDropDownConfigForUserBlogs(List<UserBlog> userBlogs)
        {
            var taskResult = await Task.Run<ResultItem<KendoDropDownConfig>>(() =>
            {

                var result = new ResultItem<KendoDropDownConfig>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new KendoDropDownConfig
                    {
                        dataSource = userBlogs.OrderBy(lookItm => lookItm.BlogName).Select(attType => new KendoDropDownItem
                        {
                            Value = attType.Guid,
                            Text = attType.BlogName
                        }).ToList(),
                        optionLabel = "Userblog"
                    }
                };
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<WebServiceItem>>> GetWebServices()
        {
            var taskResult = await Task.Run<ResultItem<List<WebServiceItem>>>(() =>
            {
                WebServiceFactory webServiceFactory = new WebServiceFactory(Globals);

                var webservices = webServiceFactory.GetData_BaseConfig();

               
                return webservices;
            });

            return taskResult;
        }

        public async Task<ResultItem<SyncWordpressPostsResult>> SyncWordpressPosts(SyncWordpressPostsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncWordpressPostsResult>>(async() =>
            {
                var result = new ResultItem<SyncWordpressPostsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SyncWordpressPostsResult()
                };

                var elasticAgent = new ServiceAgentElastic(Globals);

                request.MessageOutput?.OutputInfo("Get Model...");
                var getModelResult = await elasticAgent.GetSyncWordpressPostsModel(request);

                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Found {getModelResult.Result.Configs.Count} Configs");

                var securityConnector = new SecurityController(Globals);


                foreach (var config in getModelResult.Result.Configs)
                {
                    request.MessageOutput?.OutputInfo($"Config: {config.Name}");
                    var webServices = (from webservice in getModelResult.Result.WebServices.Where(rel => rel.ID_Object == config.GUID)
                                       join masterUser in getModelResult.Result.ConfigsToMasterUsers on webservice.ID_Object equals masterUser.ID_Object
                                       join url in getModelResult.Result.WebServicesToUrls on webservice.ID_Other equals url.ID_Object
                                       join user in getModelResult.Result.WebServicesToUsers on webservice.ID_Other equals user.ID_Object
                                       select new { webservice, masterUser, url, user }).ToList();

                    if (!webServices.Any())
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Information of Config {config.Name} is not complete!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    if (webServices.Count != 1)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Information of Config {config.Name} is redundant!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var webService = webServices.First();

                    request.MessageOutput?.OutputInfo($"Config {config.Name}: Get Password...");
                    var getPasswordResult = await securityConnector.GetPassword(new OntologyClasses.BaseClasses.clsOntologyItem
                    {
                        GUID = webService.user.ID_Other,
                        Name = webService.user.Name_Other,
                        GUID_Parent = webService.user.ID_Parent_Other,
                        Type = webService.user.Ontology
                    }, request.MasterPassword);

                    result.ResultState = getPasswordResult.Result;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = $"Config: {config.Name}: Error while getting the password!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo($"Config {config.Name}: Have Password.");

                    var credentialItem = getPasswordResult.CredentialItems.FirstOrDefault();

                    if (credentialItem == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Config: {config.Name}: No password found!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var getUserBlogsRequest = new GetUserBlogsRequest(webService.url.Name_Other, webService.user.Name_Other, credentialItem.Password.Name_Other)
                    {
                        MessageOutput = request.MessageOutput
                    };

                    request.MessageOutput?.OutputInfo($"Config {config.Name}: Get Userblogs...");

                    var getBlogsResult = await GetUserBlogs(getUserBlogsRequest);

                    result.ResultState = getBlogsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = $"Config: {config.Name}: Error while getting the Userblogs!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    var userBlog = getBlogsResult.Result.FirstOrDefault();

                    if (userBlog == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Config: {config.Name}: No Userblog found!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo($"Config {config.Name}: Have Userblog: {userBlog.BlogName}.");

                    var getPostsRequest = new GetPostListRequest(userBlog, webService.url.Name_Other, webService.user.Name_Other, credentialItem.Password.Name_Other);

                    request.MessageOutput?.OutputInfo($"Config {config.Name}: Get Posts...");

                    var getPostsResult = await GetPostList(getPostsRequest);

                    result.ResultState = getPostsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = $"Config: {config.Name}: Error while getting the Posts!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    result.Result.PostItems = getPostsResult.Result;

                    request.MessageOutput?.OutputInfo($"Config {config.Name}: Have {result.Result.PostItems.Count} Posts.");
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<UserBlog>>> GetUserBlogs(GetUserBlogsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<UserBlog>>>(() =>
            {
                var result = new ResultItem<List<UserBlog>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<UserBlog>()
                };

                var wordPressOntologyEngine = new WordPressOntologyEngine(Globals);
                request.MessageOutput?.OutputInfo("Start getting userblogs...");
                result = wordPressOntologyEngine.GetUserBlogs(new WordPress_Module.Models.GetUserBlogsRequest(request.BaseUrl,
                    request.UserName,
                    request.Password,
                    true)
                    { MessageOutput = request.MessageOutput });

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputInfo("Determined userblogs.");
                }
                
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<PostItem>>> GetPostList(GetPostListRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<PostItem>>>(() =>
            {
                var result = new ResultItem<List<PostItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<PostItem>()
                };

                var wordPressOntologyEngine = new WordPressOntologyEngine(Globals);
                result = wordPressOntologyEngine.GetPostList(new WordPress_Module.Models.GetPostListRequest(request.UserBlog,
                    request.BaseUrl,
                    request.UserName,
                    request.Password,
                    true));

                return result;
            });

            return taskResult;
        }

        public WordpressSyncConnector(Globals globals) : base(globals)
        {
        }
    }
}
