﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;

namespace WordPressSyncModule.Models
{
    public class SyncWordpressPostsResult
    {
        public List<PostItem> PostItems { get; set; } = new List<PostItem>();
    }
}
