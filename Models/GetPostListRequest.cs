﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;

namespace WordPressSyncModule.Models
{
    public class GetPostListRequest
    {
        public UserBlog UserBlog { get; private set; }
        public string BaseUrl { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public GetPostListRequest(UserBlog userBlog, string baseUrl, string userName, string password)
        {
            UserBlog = userBlog;
            BaseUrl = baseUrl;
            UserName = userName;
            Password = password;
        }
    }
}
