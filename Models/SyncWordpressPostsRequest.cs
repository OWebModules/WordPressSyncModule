﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPressSyncModule.Models
{
    public class SyncWordpressPostsRequest
    {
        public string IdConfig { get; private set; }
        public string MasterPassword { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput {get; set;}

        public SyncWordpressPostsRequest(string idConfig, string masterPassword)
        {
            IdConfig = idConfig;
            MasterPassword = masterPassword;
        }
    }
}
