﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPressSyncModule.Models
{
    public class GetSyncWordpressPostsModelResult
    {
        public clsOntologyItem BaseConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ConfigsToMasterUsers { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> WebServices { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> WebServicesToUrls { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> WebServicesToUsers { get; set; } = new List<clsObjectRel>();
    }
}
