﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPressSyncModule.Models
{
    public class GetUserBlogsRequest
    {
        public string BaseUrl { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public GetUserBlogsRequest(string baseUrl, string userName, string password)
        {
            BaseUrl = baseUrl;
            UserName = userName;
            Password = password;
        }
    }
}
