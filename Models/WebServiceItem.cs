﻿using OntologyAppDBConnector.Attributes;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordPressSyncModule.Models
{
    public class WebServiceItem : NotifyPropertyChange
    {
        private string guid;
        public string Guid
        {
            get { return guid; }
            set
            {
                guid = value;
                RaisePropertyChanged(nameof(Guid));
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        
        private clsOntologyItem userItem;
        public clsOntologyItem UserItem
        {
            get { return userItem; }
            set
            {
                userItem = value;
                RaisePropertyChanged(nameof(UserItem));
            }
        }

        private clsOntologyItem urlItem;
        public clsOntologyItem UrlItem
        {
            get { return urlItem; }
            set
            {
                urlItem = value;
                RaisePropertyChanged(nameof(UrlItem));
            }
        }

        public string Password
        {
            get; set;
        }
    }
}
