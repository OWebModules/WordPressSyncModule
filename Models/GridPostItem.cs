﻿using OntologyAppDBConnector.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;

namespace WordPressSyncModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class GridPostItem : NotifyPropertyChange
    {
        private PostItem postItem;

        [KendoColumn(hidden = true)]
        public string Guid
        {
            get { return postItem.Guid; }
            set
            {
                postItem.Guid = value;
            }
        }
        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Id")]
        public int Id
        {
            get { return int.Parse(postItem.Id); }
            set
            {
                postItem.Id = value.ToString();
            }
        }
        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Name")]
        public string Title
        {
            get { return postItem.Title; }
            set
            {
                postItem.Title = value;
            }
        }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Created", template = "#= Created != null ? kendo.toString(kendo.parseDate(Created), 'dd.MM.yyyy') : '' #")]
        public DateTime Created
        {
            get { return postItem.Created; }
            set
            {
                postItem.Created = value;
            }
        }

        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Status")]
        public string Status
        {
            get { return postItem.Status; }
            set
            {
                postItem.Status = value;
            }
        }

        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Type")]
        public string Type
        {
            get { return postItem.Type; }
            set
            {
                postItem.Type = value;
            }
        }

        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Name")]
        public string Name
        {
            get { return postItem.Name; }
            set
            {
                postItem.Name = value;
            }
        }

        [KendoColumn(hidden = true)]
        public string Author
        {
            get { return postItem.Author; }
            set
            {
                postItem.Author = value;
            }
        }

        [KendoColumn(hidden = true)]
        public string Parent
        {
            get { return postItem.Parent; }
            set
            {
                postItem.Parent = value;
            }
        }

        [KendoColumn(hidden = true)]
        public string Link
        {
            get { return postItem.Link; }
            set
            {
                postItem.Link = value;
            }
        }

        private string termItems;
        public string TermItems
        {
            get { return termItems; }
            set
            {
                termItems = value;
            }
        }

        public GridPostItem(PostItem postItem)
        {
            this.postItem = postItem;
            this.postItem.PropertyChanged += PostItem_PropertyChanged;
            TermItems = string.Join(", ", postItem.TermItems.Select(termItem => termItem.Name));
        }

        private void PostItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e.PropertyName);
            TermItems = string.Join(", ", postItem.TermItems.Select(termItem => termItem.Name));
        }
    }
}
