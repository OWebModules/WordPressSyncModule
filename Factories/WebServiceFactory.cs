﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Attributes;
using OntologyClasses.AbstractClasses;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WordPressSyncModule.Models;

namespace WordPressSyncModule.Factories
{
    public class WebServiceFactory 
    {
        private Globals globals;
        public OntologyModDBConnector dbReader_WebServices;
        public OntologyModDBConnector dbReader_Users;
        public OntologyModDBConnector dbReader_Urls;
        public OntologyModDBConnector dbReader_Passwords;

        public WebServiceFactory(Globals globals)
        {
            this.globals = globals;
            Initialize();
        }


        public ResultItem<List<WebServiceItem>> GetData_BaseConfig()
        {

            var searchWebServices = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = WordpressSync.Config.LocalData.Object_BaseConfig.GUID,
                    ID_RelationType = WordpressSync.Config.LocalData.RelationType_belonging.GUID,
                    ID_Parent_Other = WordpressSync.Config.LocalData.Class_Web_Service.GUID
                }
            };

            var result = new ResultItem<List<WebServiceItem>>
            {
                ResultState = dbReader_WebServices.GetDataObjectRel(searchWebServices)
            };

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

           

            var searchUsers = dbReader_WebServices.ObjectRels.Select(webService => new clsObjectRel
            {
                ID_Object = webService.ID_Other,
                ID_RelationType = WordpressSync.Config.LocalData.RelationType_secured_by.GUID,
                ID_Parent_Other = WordpressSync.Config.LocalData.Class_user.GUID
            }).ToList();

            result.ResultState = dbReader_Users.GetDataObjectRel(searchUsers);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var searchUrls = dbReader_WebServices.ObjectRels.Select(webService => new clsObjectRel
            {
                ID_Object = webService.ID_Other,
                ID_RelationType = WordpressSync.Config.LocalData.RelationType_accessible_by.GUID,
                ID_Parent_Other = WordpressSync.Config.LocalData.Class_user.GUID
            }).ToList();

            result.ResultState = dbReader_Urls.GetDataObjectRel(searchUrls);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var searchPassword = dbReader_Users.ObjectRels.Select(userRel => new clsObjectRel
            {
                ID_Object = userRel.ID_Other,
                ID_RelationType = WordpressSync.Config.LocalData.RelationType_secured_by.GUID
            }).ToList();

            if (searchPassword.Any())
            {
                result.ResultState = dbReader_Passwords.GetDataObjectRel(searchPassword);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = (from webService in dbReader_WebServices.ObjectRels
                           join userItem in dbReader_Users.ObjectRels on webService.ID_Other equals userItem.ID_Object
                           join urlItem in dbReader_Urls.ObjectRels on webService.ID_Other equals urlItem.ID_Object into urlItems
                           from urlItem in urlItems.DefaultIfEmpty()
                           join passwordItem in dbReader_Passwords.ObjectRels on userItem.ID_Other equals passwordItem.ID_Object into passwordItems
                           from passwordItem in passwordItems.DefaultIfEmpty()
                           select new WebServiceItem
                           {
                               Guid = webService.ID_Other,
                               Name = webService.Name_Other,
                               UrlItem = urlItem != null ? new clsOntologyItem
                               {
                                   GUID = urlItem.ID_Other,
                                   Name = urlItem.Name_Other,
                                   GUID_Parent = urlItem.ID_Parent_Other,
                                   Type = urlItem.Ontology
                               } : null,
                               UserItem = new clsOntologyItem
                               {
                                   GUID = userItem.ID_Other,
                                   Name = userItem.Name_Other,
                                   GUID_Parent = userItem.ID_Parent_Other,
                                   Type = urlItem.Ontology
                               },
                               Password = passwordItem != null ? "*****" : ""
                           }).ToList();

            //WebServices.ForEach(webService =>
            //{
            //    var password = localConfig.SecurityWork.decode_Password(webService.UserItem);

            //    if (password != null)
            //    {
            //        webService.Password = password;
            //    }
            //});

            return result;

        }

        private void Initialize()
        {
            dbReader_WebServices = new OntologyModDBConnector(globals);
            dbReader_Users = new OntologyModDBConnector(globals);
            dbReader_Urls = new OntologyModDBConnector(globals);
            dbReader_Passwords = new OntologyModDBConnector(globals);

        }
    }
}
